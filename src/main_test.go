package main_test

import (
	. "NLMK/src"
	. "NLMK/src/mock"
	"encoding/json"

	. "NLMK/src/testdata"

	"github.com/hyperledger/fabric/common/util"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("General business workflow tests", func() {

	fs := NewMockStub("testingStub", new(NLMKChaincode))

	BeforeSuite(func() {
		fs.MockStub.MockInit("000", nil)
	})

	Describe("Checking the blockchain business operations", func() {
		Context("Given that Chaincode has its interface methods well defined and working", func() {
			It("Should save an Order into the chain and receive an Order Initialised event", func() {
				// Skip("Skipping as the Events are not implemented")
				// Arrange
				orderBytes := GetCorrectOrderAsBytes("012345")
				fs.MockCreator(NlmkOrg, CertNLMKWithoutAttrs)
				privateDetailsBytes := GetPrivateDetailsAsBytes()
				fs.MockStub.PutPrivateData(FullInfo, "privateDetails", privateDetailsBytes)
				// Act
				result := fs.MockInvoke("000", util.ToChaincodeArgs("createOrder", string(orderBytes)))
				if result.Status != OK {
					Fail("Message: " + result.Message)
				}
				// Assert
				Expect(result.Status).Should(Equal(OK))

				// Arrange
				var returnedOrder Order
				returnedOrderAsBytes := []byte(result.Payload)
				err := json.Unmarshal(returnedOrderAsBytes, &returnedOrder)
				if err != nil {
					Fail("The returned Order instance presented as bytes could not be parsed")
				}

				var initEvent OrderInitialisedEvent
				channelEvent, ok := <-fs.MockStub.ChaincodeEventsChannel
				if ok {
					err := json.Unmarshal(channelEvent.Payload, &initEvent)
					if err != nil {
						Fail("The Order Init event instance could not be parsed")
					}
				}

				// Assert
				Expect(initEvent.UIN).Should(Equal(returnedOrder.UIN))
			})
			It("Should save an Order into the chain and retrieve it back successfully", func() {
				// Part 1
				// Arrange
				orderBytes := GetCorrectOrderAsBytes("0123456")
				fs.MockCreator(NlmkOrg, CertNLMKWithoutAttrs)
				privateDetailsBytes := GetPrivateDetailsAsBytes()
				fs.MockStub.PutPrivateData(FullInfo, "privateDetails", privateDetailsBytes)
				// Act
				result := fs.MockInvoke("000", util.ToChaincodeArgs("createOrder", string(orderBytes)))
				if result.Status != OK {
					Fail("Message: " + result.Message)
				}
				// Assert
				Expect(result.Status).Should(Equal(OK))

				// Part 2
				// Arrange
				var order Order
				err := json.Unmarshal(result.Payload, &order)
				if err != nil {
					Fail("The Order instance presented as bytes could not be parsed")
				}
				// Act
				result = fs.MockInvoke("000", util.ToChaincodeArgs("getOrder", order.UIN))
				// Assert
				Expect(result.Status).Should(Equal(OK))

				// Arrange
				var returnedOrder Order
				returnedOrderAsBytes := []byte(result.Payload)
				err = json.Unmarshal(returnedOrderAsBytes, &returnedOrder)
				if err != nil {
					Fail("The Order instance presented as bytes could not be parsed")
				}

				// We are not comparing the CreatedBy, CreatedAt and TxID fields, so we explicitly equalise them
				returnedOrder.CreatedBy = order.CreatedBy
				returnedOrder.CreatedAt = order.CreatedAt
				returnedOrder.TxID = order.TxID
				// We're also not comparing the Order PrivateDetails
				returnedOrder.PrivateDetails = order.PrivateDetails
				// We also construct the UIN field of the input Order
				order.UIN = ConstructUIN(order.AgreementNumber, order.InvoiceNumber, order.InvoiceDate)

				// Assert
				Expect(order).Should(Equal(returnedOrder))
			})
			It("Should be able to retrieve the created deals successfully", func() {
				Skip("Skipping the aggregation test as the shim's GetQueryResult is not implemented")
				// Arrange
				fs.MockCreator(NlmkOrg, CertNLMKWithoutAttrs)
				order1Bytes := GetCorrectOrderAsBytes("012345678")
				order2Bytes := GetCorrectOrderAsBytes("876543210")
				privateDetailsBytes := GetPrivateDetailsAsBytes()
				fs.MockStub.PutPrivateData(FullInfo, "privateDetails", privateDetailsBytes)
				// Act
				result1 := fs.MockInvoke("000", util.ToChaincodeArgs("createOrder", string(order1Bytes)))
				if result1.Status != OK {
					Fail("Message: " + result1.Message)
				}
				result2 := fs.MockInvoke("000", util.ToChaincodeArgs("createOrder", string(order2Bytes)))
				if result2.Status != OK {
					Fail("Message: " + result2.Message)
				}

				// Assert intermediate results
				Expect(result1.Status).Should(Equal(OK))
				Expect(result2.Status).Should(Equal(OK))

				// Act
				status := Initialised
				statusAsStr, _ := ConvertPaymentStatusToStr(status)
				result := fs.MockInvoke("000", util.ToChaincodeArgs("listOrders", statusAsStr))

				// Assert
				Expect(result.Status).Should(Equal(OK))
				// additional returned payload checks should be implemented here
			})
			It("Should be able to change status of an order successfully", func() {

				// Arrange
				fs.MockCreator(NlmkOrg, CertNLMKWithoutAttrs)
				orderBytes := GetCorrectOrderAsBytes("01234")
				privateDetailsBytes := GetPrivateDetailsAsBytes()
				fs.MockStub.PutPrivateData(FullInfo, "privateDetails", privateDetailsBytes)

				// Act
				result := fs.MockInvoke("000", util.ToChaincodeArgs("createOrder", string(orderBytes)))
				if result.Status != OK {
					Fail("Message: " + result.Message)
				}

				// Assert intermediate results
				Expect(result.Status).Should(Equal(OK))

				// Arrange
				var statusObj OrderStatus
				statusObj.Status = DraftCreated
				statusAsBytes, _ := json.Marshal(statusObj)
				statusAsStr := string(statusAsBytes)

				var order Order
				err := json.Unmarshal(orderBytes, &order)
				if err != nil {
					Fail("The Order instance presented as bytes could not be parsed")
				}

				// Act
				result = fs.MockInvoke("000", util.ToChaincodeArgs("changeOrderStatus", order.UIN, statusAsStr))

				// Assert
				Expect(result.Status).Should(Equal(OK))

				var publicOrder PublicOrder
				err = json.Unmarshal(result.Payload, &publicOrder)
				if err != nil {
					Fail("The Public Order instance returned as bytes could not be parsed")
				}

				Expect(publicOrder.Status).Should(Equal(DraftCreated))
			})

			It("Should retrieve history of an Order by key", func() {
				Skip("Skipping the retrieve history test as the shim's GetHistoryForKey is not implemented")

				// Arrange
				fs.MockCreator(NlmkOrg, CertNLMKWithoutAttrs)
				orderBytes := GetCorrectOrderAsBytes("01234")

				var order Order
				err := json.Unmarshal(orderBytes, &order)
				if err != nil {
					Fail("The Order instance presented as bytes could not be parsed")
				}
				invoiceDateAsStr := ConvertDateToLongStr(order.InvoiceDate)

				// Act
				result := fs.MockInvoke("000", util.ToChaincodeArgs("getHistoryByKey", order.AgreementNumber, order.InvoiceNumber, invoiceDateAsStr))

				// Assert
				Expect(result.Status).Should(Equal(OK))
			})

			It("Should correctly assess status change according to schema", func() {
				// start with positive scenarios
				Expect(ValidateStatusChange(Initialised, DraftCreated)).Should(Equal(true))
				Expect(ValidateStatusChange(DraftCreated, PaidToBank)).Should(Equal(true))
				Expect(ValidateStatusChange(PaidToBank, ApprovedByBank)).Should(Equal(true))
				Expect(ValidateStatusChange(PaidToBank, DeclinedByBank)).Should(Equal(true))
				Expect(ValidateStatusChange(ApprovedByBank, Finalised)).Should(Equal(true))
				Expect(ValidateStatusChange(DeclinedByBank, Finalised)).Should(Equal(true))
				// and finish off with a few negative ones
				Expect(ValidateStatusChange(Initialised, ApprovedByBank)).Should(Equal(false))
				Expect(ValidateStatusChange(Initialised, Initialised)).Should(Equal(false))
				Expect(ValidateStatusChange(Initialised, Finalised)).Should(Equal(false))
				Expect(ValidateStatusChange(PaidToBank, Finalised)).Should(Equal(false))
				Expect(ValidateStatusChange("", "")).Should(Equal(false))
				Expect(ValidateStatusChange("unknown", "unknown")).Should(Equal(false))
			})
		})
	})

	Describe("Checking the blockchain input validations", func() {
		Context("Given that Chaincode is able to detect incorrect input data", func() {
			It("Should not allow saving an Order with INN length not equal to 10", func() {
				// Part 1
				// Arrange
				orderBytes := GetCorrectOrderAsBytes("01234567")
				fs.MockCreator(NlmkOrg, CertNLMKWithoutAttrs)
				privateDetailsBytes := GetPrivateDetailsAsBytes()
				var privateDetails PrivateDetails
				_ = json.Unmarshal(privateDetailsBytes, &privateDetails)
				privateDetails.Customer.INN = "0123456789012" // the field value is too long
				privateDetailsBytes, _ = json.Marshal(privateDetails)

				fs.MockStub.PutPrivateData(FullInfo, "privateDetails", privateDetailsBytes)
				// Act
				result := fs.MockInvoke("000", util.ToChaincodeArgs("createOrder", string(orderBytes)))
				if result.Status != BAD_REQUEST {
					Fail("Message: " + result.Message)
				}
				// Assert
				Expect(result.Status).Should(Equal(BAD_REQUEST))
				Expect(result.Message).Should(Equal("INN must consist of 10 digits"))
			})

			It("Should not allow saving an Order with INN containing alphanumeric symbols", func() {
				// Part 1
				// Arrange
				orderBytes := GetCorrectOrderAsBytes("01234567")
				privateDetailsBytes := GetPrivateDetailsAsBytes()
				var privateDetails PrivateDetails
				_ = json.Unmarshal(privateDetailsBytes, &privateDetails)
				privateDetails.Customer.INN = "01234567890b" // the field contains an invalid symbol
				privateDetailsBytes, _ = json.Marshal(privateDetails)
				fs.MockStub.PutPrivateData(FullInfo, "privateDetails", privateDetailsBytes)
				fs.MockCreator(NlmkOrg, CertNLMKWithoutAttrs)
				// Act
				result := fs.MockInvoke("000", util.ToChaincodeArgs("createOrder", string(orderBytes)))
				if result.Status != BAD_REQUEST {
					Fail("Message: " + result.Message)
				}
				// Assert
				Expect(result.Status).Should(Equal(BAD_REQUEST))
				Expect(result.Message).Should(Equal("INN must only contain digits"))
			})

			It("Should not allow saving an Order with empty INN", func() {
				// Part 1
				// Arrange
				orderBytes := GetCorrectOrderAsBytes("01234567")
				privateDetailsBytes := GetPrivateDetailsAsBytes()
				var privateDetails PrivateDetails
				_ = json.Unmarshal(privateDetailsBytes, &privateDetails)
				privateDetails.Customer.INN = "" // the field is blank
				privateDetailsBytes, _ = json.Marshal(privateDetails)
				fs.MockStub.PutPrivateData(FullInfo, "privateDetails", privateDetailsBytes)

				fs.MockCreator(NlmkOrg, CertNLMKWithoutAttrs)

				// Act
				result := fs.MockInvoke("000", util.ToChaincodeArgs("createOrder", string(orderBytes)))
				if result.Status != BAD_REQUEST {
					Fail("Message: " + result.Message)
				}
				// Assert
				Expect(result.Status).Should(Equal(BAD_REQUEST))
				Expect(result.Message).Should(Equal("INN is blank"))
			})
		})
	})

	Describe("Checking the public/private data access validations", func() {
		Context("Given that Chaincode is able to determine caller's Organisation", func() {
			It("Should correctly grant access control to different organisations", func() {
				Expect(AccessControl(FullInfo, NlmkOrg)).Should(Equal(true))
				Expect(AccessControl(FullInfo, LogisticOrg)).Should(Equal(false))
				Expect(AccessControl(FullInfo, "")).Should(Equal(false))
				Expect(AccessControl("", "")).Should(Equal(false))
				Expect(AccessControl("unknown", "unknown")).Should(Equal(false))
			})

			It("Should NOT allow the 'logistic' org to invoke the createOrder", func() {
				// Arrange
				orderBytes := GetCorrectOrderAsBytes("01234567890")
				fs.MockCreator(LogisticOrg, CertLogisticWithAttrs)
				privateDetailsBytes := GetPrivateDetailsAsBytes()
				fs.MockStub.PutPrivateData(FullInfo, "privateDetails", privateDetailsBytes)
				// Act
				result := fs.MockInvoke("000", util.ToChaincodeArgs("createOrder", string(orderBytes)))
				// Assert
				Expect(result.Status).Should(Equal(NOT_AUTHORISED))
			})

			It("Should allow the 'nlmk' org to invoke the createOrder", func() {
				// Arrange
				orderBytes := GetCorrectOrderAsBytes("01234567890")
				fs.MockCreator(NlmkOrg, CertNLMKWithoutAttrs)
				privateDetailsBytes := GetPrivateDetailsAsBytes()
				fs.MockStub.PutPrivateData(FullInfo, "privateDetails", privateDetailsBytes)
				// Act
				result := fs.MockInvoke("000", util.ToChaincodeArgs("createOrder", string(orderBytes)))
				// Assert
				Expect(result.Status).Should(Equal(OK))
			})

			It("Should allow the 'nlmk' org to invoke the createOrder and read the full information", func() {
				// Assert
				Skip("This save/retrieve functionality has already been tested above")
			})

			It("Should allow the 'logistic' org to only read the public part of the created order", func() {
				// Part 1. Create an Order by the 'nlmk' org
				// Arrange
				orderBytes := GetCorrectOrderAsBytes("0123456789")
				fs.MockCreator(NlmkOrg, CertNLMKWithoutAttrs)
				privateDetailsBytes := GetPrivateDetailsAsBytes()
				fs.MockStub.PutPrivateData(FullInfo, "privateDetails", privateDetailsBytes)
				// Act
				result := fs.MockInvoke("000", util.ToChaincodeArgs("createOrder", string(orderBytes)))
				// Assert
				Expect(result.Status).Should(Equal(OK))

				// Part 2. Read the Order by the 'logistic' org
				// Arrange
				var order Order
				err := json.Unmarshal(orderBytes, &order)
				if err != nil {
					Fail("The Order instance presented as bytes could not be parsed")
				}
				// invoiceDateAsStr := ConvertDateToLongStr(order.InvoiceDate)
				fs.MockCreator(LogisticOrg, CertLogisticWithAttrs)
				// Act
				result = fs.MockInvoke("000", util.ToChaincodeArgs("getOrder", order.UIN))
				//order.AgreementNumber, order.InvoiceNumber, invoiceDateAsStr))
				// Assert
				Expect(result.Status).Should(Equal(OK))

				// Arrange
				var returnedOrder Order
				returnedOrderAsBytes := []byte(result.Payload)
				err = json.Unmarshal(returnedOrderAsBytes, &returnedOrder)
				if err != nil {
					Fail("The Order instance presented as bytes could not be parsed")
				}
				// create an empty PrivateDetails instance to be compared with the one in the returned order
				var privateDetails PrivateDetails
				_ = json.Unmarshal([]byte{}, &privateDetails)

				// Assert
				Expect(returnedOrder.PrivateDetails).Should(Equal(privateDetails)) // here the two 'default' objects are compared
				Expect(returnedOrder.AgreementNumber).Should(Equal(order.AgreementNumber))
				Expect(returnedOrder.InvoiceNumber).Should(Equal(order.InvoiceNumber))
				Expect(returnedOrder.InvoiceDate).Should(Equal(order.InvoiceDate))
				Expect(returnedOrder.DeliveryAddress).Should(Equal(order.DeliveryAddress))
				Expect(returnedOrder.Status).Should(Equal(order.Status))
			})
		})
	})
})
