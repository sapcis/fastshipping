package main

import (
	"encoding/json"
	"errors"
	"strings"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

// var keyPrefix = "nlmk"
const msgElementNotFound = "Element not found"
const msgElementAlreadyExists = "Element already exists"
const msgPrivateElementNotFound = "Element not found in private data"

// WriteStrategy reflects different strategies for writing to ledger
type WriteStrategy int

func writeElementToLedger(stub shim.ChaincodeStubInterface, key string, element interface{}, writeStrategy WriteStrategy) ([]byte, error) {
	logger.Info("Saving element to ledger with key " + key)

	//Convert to bytes
	elementAsBytes, err := json.Marshal(element)
	if err != nil {
		return nil, err
	}

	//Write to ledger
	_, err = writeBytesToLedger(stub, key, elementAsBytes, writeStrategy)
	if err != nil {
		return nil, err
	}

	return elementAsBytes, nil
}

func writeBytesToLedger(stub shim.ChaincodeStubInterface, key string, bytes []byte, writeStrategy WriteStrategy) ([]byte, error) {
	if writeStrategy == SKIP || writeStrategy == THROW {
		//Check if entry already exists
		exists, err := checkExistsOnLedger(stub, key)
		if err != nil {
			return nil, err
		}

		if exists {
			if writeStrategy == THROW {
				return nil, errors.New(msgElementAlreadyExists)
			} else if writeStrategy == SKIP {
				return nil, nil
			}
		}
	}

	//Write to ledger
	err := stub.PutState(key, bytes)
	if err != nil {
		return nil, err
	}

	return bytes, nil
}

func readElementFromLedger(stub shim.ChaincodeStubInterface, key string) ([]byte, error) {
	logger.Info("Getting element from ledger with key " + key)

	elementAsBytes, err := stub.GetState(key)
	if err != nil {
		return nil, err
	}

	if elementAsBytes == nil || len(elementAsBytes) == 0 {
		logger.Debug("Could not find element on ledger with key " + key)
		return nil, errors.New(msgElementNotFound)
	}

	return elementAsBytes, nil
}

func writeElementToPrivateData(stub shim.ChaincodeStubInterface, key string, element interface{}) ([]byte, error) {
	logger.Info("Saving element to private data with key " + key)
	// convert to bytes
	elementAsBytes, err := json.Marshal(element)
	if err != nil {
		return nil, err
	}

	// save to private data
	err = stub.PutPrivateData(FullInfo, key, elementAsBytes)
	if err != nil {
		return nil, err
	}

	return elementAsBytes, nil
}

func readElementFromPrivateData(stub shim.ChaincodeStubInterface, key string) ([]byte, error) {
	logger.Info("Getting element from private data with key " + key)

	elementAsBytes, err := stub.GetPrivateData(FullInfo, key)
	if err != nil {
		logger.Debug("Could not get private data element with key " + key)
		return nil, err
	}

	if elementAsBytes == nil || len(elementAsBytes) == 0 {
		logger.Debug("Could not find private data element with key " + key)
		return nil, errors.New(msgPrivateElementNotFound)
	}

	return elementAsBytes, nil
}

func checkExistsOnLedger(stub shim.ChaincodeStubInterface, key string) (bool, error) {
	logger.Debug("Checking if element exists on ledger with key " + key)
	_, err := readElementFromLedger(stub, key)
	if err != nil {
		logger.Debug("Error getting element from ledger with key " + key)
		if strings.Contains(err.Error(), msgElementNotFound) {
			return false, nil
		}

		return false, err
	}

	return true, nil
}
