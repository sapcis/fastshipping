package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"time"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/protos/peer"
)

var logger = shim.NewLogger("NLMKChaincode")

// NLMKChaincode ...
type NLMKChaincode struct {
}

func main() {
	if err := shim.Start(new(NLMKChaincode)); err != nil {
		logger.Error(fmt.Sprintf("Error starting chaincode: %s", err))
	}
}

// Init - initialize the chaincode
func (t *NLMKChaincode) Init(stub shim.ChaincodeStubInterface) peer.Response {
	logger.SetLevel(shim.LogDebug)
	return shim.Success(nil)
}

// Invoke - entry point for Invocations
func (t *NLMKChaincode) Invoke(stub shim.ChaincodeStubInterface) peer.Response {
	function, args := stub.GetFunctionAndParameters()
	logger.Debug(fmt.Sprintf("Invoke is running %v with args %v", function, args))

	transient, err := stub.GetTransient()
	if err != nil {
		return BadRequest("Error getting transient data from request")
	}

	// Handle different functions
	switch function {

	case "createOrder":
		return createOrder(stub, args, transient)
	case "getOrder":
		return getOrder(stub, args, transient)
	case "listOrders":
		return listOrders(stub, args, transient)
	case "changeOrderStatus":
		return changeOrderStatus(stub, args, transient)
	case "getHistoryByKey":
		return getHistoryByKey(stub, args, transient)
	default:
		return shim.Error("Invoke: no such method " + function)

	}
}

func createOrder(stub shim.ChaincodeStubInterface, args []string, transient map[string][]byte) peer.Response {

	// TODO: add org/certificate validation

	createdBy, err := ValidateAccess(stub)
	if err != nil {
		return NotAuthorised(err.Error())
	}

	err = validateNumberOfArgumentsWithLengthCheck(1, args)
	if err != nil {
		return BadRequest(err.Error())
	}

	order, err := extractOrderFromArgs(stub, args, transient)
	if err != nil {
		return BadRequest(err.Error())
	}

	// sorry, it's probably not the most efficient code below, but I wanted to make
	// it obvious that the public part of the order doesn't get saved into the ledger
	var publicOrder PublicOrder
	publicOrder.AgreementNumber = order.AgreementNumber
	publicOrder.InvoiceNumber = order.InvoiceNumber
	publicOrder.InvoiceDate = order.InvoiceDate
	publicOrder.UIN = order.UIN
	publicOrder.TxID = stub.GetTxID()
	publicOrder.DeliveryAddress = order.DeliveryAddress
	publicOrder.Status = Initialised
	publicOrder.CreatedBy = createdBy
	timestamp, _ := stub.GetTxTimestamp()
	publicOrder.CreatedAt = time.Unix(timestamp.Seconds, int64(timestamp.Nanos))
	order.CreatedAt = publicOrder.CreatedAt
	// the UpdatedBy and UpdatedAt fields are staying blank when the order is being created

	orderKey, err := extractOrderKeyFromOrder(order)
	if err != nil {
		return Error(err.Error())
	}

	key, err := stub.CreateCompositeKey(prefixOrder, []string{orderKey})
	if err != nil {
		return Error(err.Error())
	}
	// remove the nil byte in the key
	key = string(bytes.Replace([]byte(key), []byte(nilByte), []byte(emptyString), -1))

	retPublicOrderAsBytes, err := writeElementToLedger(stub, key, publicOrder, THROW)
	if err != nil {
		return Error(err.Error())
	}

	_, err = writeElementToPrivateData(stub, key, order.PrivateDetails)
	if err != nil {
		return Error(err.Error())
	}

	err = raiseOrderInitEvent(stub, order)
	if err != nil {
		logger.Debug("Error raising an Order Init event: " + err.Error())
	}

	return Ok(retPublicOrderAsBytes)
}

func getOrder(stub shim.ChaincodeStubInterface, args []string, transient map[string][]byte) peer.Response {

	// TODO: add org/certificate validation

	err := validateNumberOfArgumentsWithLengthCheck(1, args)
	if err != nil {
		return BadRequest(err.Error())
	}

	orderKey, err := extractOrderKeyFromArgs(args)
	if err != nil {
		return Error(err.Error())
	}

	key, err := stub.CreateCompositeKey(prefixOrder, orderKey)
	if err != nil {
		return Error(err.Error())
	}
	key = string(bytes.Replace([]byte(key), []byte(nilByte), []byte(emptyString), -1))

	publicOrderAsBytes, err := readElementFromLedger(stub, key)
	if err != nil {
		return NotFound(err.Error())
	}

	var publicOrder PublicOrder
	err = json.Unmarshal(publicOrderAsBytes, &publicOrder)
	if err != nil {
		return Error(err.Error())
	}

	_, err = ValidateAccess(stub)
	if err != nil {
		orderAsBytes, err := json.Marshal(publicOrder)
		if err != nil {
			return Error(err.Error())
		}
		return Ok(orderAsBytes)
	}

	var orderAsBytes []byte
	privateDetailsAsBytes, err := readElementFromPrivateData(stub, key)
	if err == nil {
		var privateDetails PrivateDetails
		err = json.Unmarshal(privateDetailsAsBytes, &privateDetails)
		if err != nil {
			return Error(err.Error())
		}
		var order Order
		order.AgreementNumber = publicOrder.AgreementNumber
		order.InvoiceNumber = publicOrder.InvoiceNumber
		order.InvoiceDate = publicOrder.InvoiceDate
		order.UIN = publicOrder.UIN
		order.TxID = publicOrder.TxID
		order.DeliveryAddress = publicOrder.DeliveryAddress
		order.Status = publicOrder.Status
		order.CreatedBy = publicOrder.CreatedBy
		order.CreatedAt = publicOrder.CreatedAt
		order.UpdatedBy = publicOrder.UpdatedBy
		order.UpdatedAt = publicOrder.UpdatedAt
		order.PrivateDetails = privateDetails
		orderAsBytes, err = json.Marshal(order)
		if err != nil {
			return Error(err.Error())
		}
	}

	return Ok(orderAsBytes)
}

func listOrders(stub shim.ChaincodeStubInterface, args []string, transient map[string][]byte) peer.Response {
	// TODO: add org/certificate validation

	_, err := ValidateAccess(stub)
	if err != nil {
		return NotAuthorised(err.Error())
	}

	err = validateNumberOfArgumentsWithLengthCheck(1, args)
	if err != nil {
		logger.Debug("Error validating number of arguments. Receive " + string(len(args)))
		return BadRequest(err.Error())
	}

	_, err = convertStringArgToPaymentStatus(args[0])
	if err != nil {
		logger.Debug("Error validating Status as argument. Receive " + args[0])
		return BadRequest(err.Error())
	}

	// here the term 'deal' equals to 'order'/'agreement'
	dealsIterator, err := queryLedgerByStatus(stub, args)
	if err != nil {
		return Error(err.Error())
	}

	dealsAsBytes, err := listOrdersFromIterator(stub, dealsIterator)
	if err != nil {
		return Error(err.Error())
	}

	return Ok(dealsAsBytes)
}

func changeOrderStatus(stub shim.ChaincodeStubInterface, args []string, transient map[string][]byte) peer.Response {
	// TODO: add org/certificate validation

	updatedBy, err := ValidateAccess(stub)
	if err != nil {
		return NotAuthorised(err.Error())
	}

	err = validateNumberOfArgumentsWithLengthCheck(2, args)
	if err != nil {
		return BadRequest(err.Error())
	}

	orderKey, err := extractOrderKeyFromArgs(args)
	if err != nil {
		return Error(err.Error())
	}

	key, err := stub.CreateCompositeKey(prefixOrder, orderKey)
	if err != nil {
		return Error(err.Error())
	}
	key = string(bytes.Replace([]byte(key), []byte(nilByte), []byte(emptyString), -1))

	publicOrderAsBytes, err := readElementFromLedger(stub, key)
	if err != nil {
		return NotFound(err.Error())
	}

	var publicOrder PublicOrder
	err = json.Unmarshal(publicOrderAsBytes, &publicOrder)
	if err != nil {
		return Error(err.Error())
	}

	newStatus, err := convertArgToPaymentStatus(args[1])
	if err != nil {
		logger.Debug("Error converting arg to the payment status")
		return BadRequest(err.Error())
	}

	isStatusChangeValid, err := ValidateStatusChange(publicOrder.Status, newStatus)
	if err != nil {
		return Error(err.Error())
	}
	if !isStatusChangeValid {
		logger.Debug("Status change is not allowed")
		return BadRequest("Status change is not allowed")
	}

	publicOrder.Status = newStatus
	publicOrder.UpdatedBy = updatedBy
	timestamp, _ := stub.GetTxTimestamp()
	publicOrder.UpdatedAt = time.Unix(timestamp.Seconds, int64(timestamp.Nanos))
	publicOrder.TxID = stub.GetTxID()

	logger.Debug(fmt.Sprintf("Invoke is running changeOrderStatus with args %v", publicOrder))
	retPublicOrderAsBytes, err := writeElementToLedger(stub, key, publicOrder, UPDATE_EXISTING)
	if err != nil {
		return Error(err.Error())
	}

	return Ok(retPublicOrderAsBytes)
}

func getHistoryByKey(stub shim.ChaincodeStubInterface, args []string, transient map[string][]byte) peer.Response {

	_, err := ValidateAccess(stub)
	if err != nil {
		return NotAuthorised(err.Error())
	}

	err = validateNumberOfArgumentsWithLengthCheck(1, args)
	if err != nil {
		return BadRequest(err.Error())
	}

	orderKey, err := extractOrderKeyFromArgs(args)
	if err != nil {
		return Error(err.Error())
	}

	key, err := stub.CreateCompositeKey(prefixOrder, orderKey)
	if err != nil {
		return Error(err.Error())
	}
	key = string(bytes.Replace([]byte(key), []byte(nilByte), []byte(emptyString), -1))

	orderHistoryAsBytes, err := getOrderHistoryAsBytes(stub, key)
	if err != nil {
		return Error(err.Error())
	}

	return Ok(orderHistoryAsBytes)
}
