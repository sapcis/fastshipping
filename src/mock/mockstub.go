package mock

import (
	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/msp"
	pb "github.com/hyperledger/fabric/protos/peer"
)

// MockStub object for testing
type MockStub struct {
	shim.MockStub
	cc      shim.Chaincode
	creator []byte
}

type mockStub struct {
	creator []byte
}

// NewMockStub instantiates the MockStub for testing
func NewMockStub(name string, cc shim.Chaincode) *MockStub {
	return &MockStub{
		MockStub: *shim.NewMockStub(name, cc),
		cc:       cc,
	}
}

// MockCreator of a tx
func (stub *MockStub) MockCreator(mspID string, cert string) {
	stub.creator, _ = msp.NewSerializedIdentity(mspID, []byte(cert))
}

// GetCreator of a tx
func (stub *MockStub) GetCreator() ([]byte, error) {
	return stub.creator, nil
}

func (stub *MockStub) MockInit(uuid string, args [][]byte) pb.Response {
	// this is a hack here to set MockStub.args, because its not accessible otherwise
	stub.MockStub.MockInvoke(uuid, args)

	stub.MockTransactionStart(uuid)
	res := stub.cc.Init(stub)
	stub.MockTransactionEnd(uuid)

	return res
}

func (stub *MockStub) MockInvoke(uuid string, args [][]byte) pb.Response {
	// this is a hack here to set MockStub.args, because its not accessible otherwise
	stub.MockStub.MockInvoke(uuid, args)

	// now do the invoke with the correct stub
	stub.MockTransactionStart(uuid)
	res := stub.cc.Invoke(stub)
	stub.MockTransactionEnd(uuid)

	return res
}

// func GetMockCidStub(org string, cert string) (cid.ChaincodeStubInterface, error) {
// 	stub := &mockStub{}
// 	sid := &pmsp.SerializedIdentity{Mspid: org,
// 		IdBytes: []byte(cert)}
// 	b, err := proto.Marshal(sid)
// 	if err != nil {
// 		return nil, err
// 	}
// 	stub.creator = b
// 	return stub, nil
// }

// func getMockStubWithAttrs() (cid.ChaincodeStubInterface, error) {
// 	stub := &mockStub{}
// 	sid := &pmsp.SerializedIdentity{Mspid: "SampleOrg",
// 		IdBytes: []byte(certWithAttrs)}
// 	b, err := proto.Marshal(sid)
// 	if err != nil {
// 		return nil, err
// 	}
// 	stub.creator = b
// 	return stub, nil
// }
