package testdata

import (
	"time"
)

// const TestUser1CN = "testUser"
// const TestUser2CN = "testUser2"
// const TestUser3CN = "testUser3"

// const TestUser1Cert = `-----BEGIN CERTIFICATE-----
// MIIB9DCCAZqgAwIBAgIUDda1JZnuPZ5dlcwSlOmU/KWSn7MwCgYIKoZIzj0EAwIw
// fzELMAkGA1UEBhMCVVMxEzARBgNVBAgTCkNhbGlmb3JuaWExFjAUBgNVBAcTDVNh
// biBGcmFuY2lzY28xHzAdBgNVBAoTFkludGVybmV0IFdpZGdldHMsIEluYy4xDDAK
// BgNVBAsTA1dXVzEUMBIGA1UEAxMLZXhhbXBsZS5jb20wHhcNMTcwMjEzMTQyOTAw
// WhcNMTgwMTEyMjIyOTAwWjATMREwDwYDVQQDEwh0ZXN0VXNlcjBZMBMGByqGSM49
// AgEGCCqGSM49AwEHA0IABKqm8JxN53RW1/muhqPxO7F7dnEMhguy23MVj4CXybqP
// rY70z4AJdXKZTxPeU06kIwb1c0NMii+NMUAjp624z0qjYDBeMA4GA1UdDwEB/wQE
// AwICBDAMBgNVHRMBAf8EAjAAMB0GA1UdDgQWBBT6YW1Vq07nRK502xj3Y76/lqsu
// 3zAfBgNVHSMEGDAWgBQXZ0I9qp6CP8TFHZ9bw5nRtZxIEDAKBggqhkjOPQQDAgNI
// ADBFAiEA5tzFnCPvASFWQku49vrGNGhmJeASlbo2W1ipWarkTlQCIHpI4eWFj6na
// 4Xtb5djZAMGlfC2jJl/FTKzFj/xd4s3E
// -----END CERTIFICATE-----`

// const TestUser2Cert = `
// -----BEGIN CERTIFICATE-----
// MIIB9TCCAZugAwIBAgIUSkK6FlbMHMj8lUytz1/l0IJPw7swCgYIKoZIzj0EAwIw
// fzELMAkGA1UEBhMCVVMxEzARBgNVBAgTCkNhbGlmb3JuaWExFjAUBgNVBAcTDVNh
// biBGcmFuY2lzY28xHzAdBgNVBAoTFkludGVybmV0IFdpZGdldHMsIEluYy4xDDAK
// BgNVBAsTA1dXVzEUMBIGA1UEAxMLZXhhbXBsZS5jb20wHhcNMTcwMzEzMTAxODAw
// WhcNMTgwMjA5MTgxODAwWjAUMRIwEAYDVQQDEwl0ZXN0VXNlcjIwWTATBgcqhkjO
// PQIBBggqhkjOPQMBBwNCAAQYEbEXfqVfArb9u2p8JHiqSwEiE9cQ5mn9CKr76prT
// yjZYVmYnImQparjDhtYfiab2cEJaOqJ2J7Au16C6jJ/so2AwXjAOBgNVHQ8BAf8E
// BAMCAgQwDAYDVR0TAQH/BAIwADAdBgNVHQ4EFgQUN0EDf71qzYVtIxW5PQQzFvje
// 7L8wHwYDVR0jBBgwFoAUF2dCPaqegj/ExR2fW8OZ0bWcSBAwCgYIKoZIzj0EAwID
// SAAwRQIhAPmJKZYTYiJwvtHbG41XAeIBytyEYA0usiLEvevhN1oFAiB+sLNsJ5Y+
// BtcMVPta45X0/aZ5oyI/IJYFWBGSpvgyRQ==
// -----END CERTIFICATE-----`

// const TestUser3Cert = `
// -----BEGIN CERTIFICATE-----
// MIIB9DCCAZugAwIBAgIUSC46fLwShh0o0HEzRpvqBe0LEZ0wCgYIKoZIzj0EAwIw
// fzELMAkGA1UEBhMCVVMxEzARBgNVBAgTCkNhbGlmb3JuaWExFjAUBgNVBAcTDVNh
// biBGcmFuY2lzY28xHzAdBgNVBAoTFkludGVybmV0IFdpZGdldHMsIEluYy4xDDAK
// BgNVBAsTA1dXVzEUMBIGA1UEAxMLZXhhbXBsZS5jb20wHhcNMTcwMzEzMTAxOTAw
// WhcNMTgwMjA5MTgxOTAwWjAUMRIwEAYDVQQDEwl0ZXN0VXNlcjMwWTATBgcqhkjO
// PQIBBggqhkjOPQMBBwNCAAQaCAMezFMF7K1xwBy6pR9LP/zVKo/Nh45LMqAuM2IL
// mE1ZTFCqc1HJ3ijiSyid+uMOQyo9Jdu2ylj2qECEwYoRo2AwXjAOBgNVHQ8BAf8E
// BAMCAgQwDAYDVR0TAQH/BAIwADAdBgNVHQ4EFgQUcXgf0aeO1taJyaDs0c2B274w
// h1gwHwYDVR0jBBgwFoAUF2dCPaqegj/ExR2fW8OZ0bWcSBAwCgYIKoZIzj0EAwID
// RwAwRAIgJP9ARAqRHl6f2KxB+YJ6ICA9YyYAEkqRnBY4UcTMSIUCID7LFYDewEj3
// LmQ6Yvctwv0WEeTCLAuRSmPZL9+hNzX+
// -----END CERTIFICATE-----`

// CertLogisticWithAttrs contains Anton2@logistic-addsh.fabric.eu.icn.engineering
// with additional attribute {"genOrg":"logistic"}
const CertLogisticWithAttrs = `
-----BEGIN CERTIFICATE-----
MIID5TCCA4ygAwIBAgIRAND7joM+5kPMjAIb2qpJBHYwCgYIKoZIzj0EAwIwgZAx
CzAJBgNVBAYTAkRFMQ8wDQYDVQQKDAZTQVAgU0UxMTAvBgNVBAsMKElDTi05ZThm
ODBiMC1iNTQ0LTQ3ZWItYTAyNi1hM2IyNzVlNmNhNzUxPTA7BgNVBAMMNDllOGY4
MGIwLWI1NDQtNDdlYi1hMDI2LWEzYjI3NWU2Y2E3NS4weDNmOGEzZGIyLXBlZXIw
HhcNMTkxMTE2MTE1NTQzWhcNMjAxMTE1MTE1NTQzWjCBizELMAkGA1UEBhMCREUx
DzANBgNVBAoMBlNBUCBTRTExMC8GA1UECwwoSUNOLTllOGY4MGIwLWI1NDQtNDdl
Yi1hMDI2LWEzYjI3NWU2Y2E3NTE4MDYGA1UEAwwvQW50b24yQGxvZ2lzdGljLWFk
ZHNoLmZhYnJpYy5ldS5pY24uZW5naW5lZXJpbmcwWTATBgcqhkjOPQIBBggqhkjO
PQMBBwNCAAQk9kyuWcme+RxMOXdQYZCN9RAavAtxWnSQ3JpTKa5734f2/MeOFAwD
dGda2p/1RwsCxqp0ljUUv5nSOBQb2Dopo4IByDCCAcQwgZUGA1UdHwSBjTCBijCB
h6CBhKCBgYZ/aHR0cHM6Ly9ibG9ja2NoYWluLWNlcnRpZmljYXRlLWF1dGhvcml0
eS5jZmFwcHMuZXUxMC5oYW5hLm9uZGVtYW5kLmNvbS9wdWJsaWMvdjEvY2EvNWFj
MWUzMTgtNDgwMi00ZTRlLWE0YjYtYWY0OWYwY2ZiNjYwL2NhLmNybDCBngYIKwYB
BQUHAQEEgZEwgY4wgYsGCCsGAQUFBzAChn9odHRwczovL2Jsb2NrY2hhaW4tY2Vy
dGlmaWNhdGUtYXV0aG9yaXR5LmNmYXBwcy5ldTEwLmhhbmEub25kZW1hbmQuY29t
L3B1YmxpYy92MS9jYS81YWMxZTMxOC00ODAyLTRlNGUtYTRiNi1hZjQ5ZjBjZmI2
NjAvY2EuY3J0MAwGA1UdEwEB/wQCMAAwDgYDVR0PAQH/BAQDAgeAMB0GA1UdDgQW
BBS4widN8cbUT0i0XKmnvN6YvqSuUzAfBgNVHSMEGDAWgBSu346bSqMsaBcoFjMs
VECm1XQc0jArBggqAwQFBgcIAQQfeyJhdHRycyI6eyJnZW5PcmciOiJsb2dpc3Rp
YyJ9fTAKBggqhkjOPQQDAgNHADBEAiBbDajc3U+962cPVtuC7ziKOGCtvFV50X7Q
r7cBGVxvbAIgbMu3ML2+aN+6QGZx2aSqnSfl5pGyVbnLcIxOLl4oUp4=
-----END CERTIFICATE-----`

// CertNLMKWithoutAttrs contains Anton@nlmk-avydr.fabric.eu.icn.engineering
// without additional attributes
const CertNLMKWithoutAttrs = `
-----BEGIN CERTIFICATE-----
MIIDsjCCA1mgAwIBAgIQFNi0hyLmRx+ZNQQWpXf8szAKBggqhkjOPQQDAjCBkDEL
MAkGA1UEBhMCREUxDzANBgNVBAoMBlNBUCBTRTExMC8GA1UECwwoSUNOLTllOGY4
MGIwLWI1NDQtNDdlYi1hMDI2LWEzYjI3NWU2Y2E3NTE9MDsGA1UEAww0OWU4Zjgw
YjAtYjU0NC00N2ViLWEwMjYtYTNiMjc1ZTZjYTc1LjB4ODMzMzZjNWItcGVlcjAe
Fw0xOTExMTEwMTE3NDVaFw0yMDExMTAwMTE3NDVaMIGGMQswCQYDVQQGEwJERTEP
MA0GA1UECgwGU0FQIFNFMTEwLwYDVQQLDChJQ04tOWU4ZjgwYjAtYjU0NC00N2Vi
LWEwMjYtYTNiMjc1ZTZjYTc1MTMwMQYDVQQDDCpBbnRvbkBubG1rLWF2eWRyLmZh
YnJpYy5ldS5pY24uZW5naW5lZXJpbmcwWTATBgcqhkjOPQIBBggqhkjOPQMBBwNC
AAT72vJl3+cgPidTTApwZRXm4PTguQrGLLFxlqIqQQa0F7TySdNNX/PROsPJh8q9
1Z/G5DSOxmulKBnVxs8gsM8co4IBmzCCAZcwgZUGA1UdHwSBjTCBijCBh6CBhKCB
gYZ/aHR0cHM6Ly9ibG9ja2NoYWluLWNlcnRpZmljYXRlLWF1dGhvcml0eS5jZmFw
cHMuZXUxMC5oYW5hLm9uZGVtYW5kLmNvbS9wdWJsaWMvdjEvY2EvNDA5YTk2M2Ut
Njg4NC00ZTE1LTlkZmQtN2ZhN2ZkNWY5MzJlL2NhLmNybDCBngYIKwYBBQUHAQEE
gZEwgY4wgYsGCCsGAQUFBzAChn9odHRwczovL2Jsb2NrY2hhaW4tY2VydGlmaWNh
dGUtYXV0aG9yaXR5LmNmYXBwcy5ldTEwLmhhbmEub25kZW1hbmQuY29tL3B1Ymxp
Yy92MS9jYS80MDlhOTYzZS02ODg0LTRlMTUtOWRmZC03ZmE3ZmQ1ZjkzMmUvY2Eu
Y3J0MAwGA1UdEwEB/wQCMAAwDgYDVR0PAQH/BAQDAgeAMB0GA1UdDgQWBBRaC3EZ
fZIx/M+nuXGIRp/wAgu35jAfBgNVHSMEGDAWgBRan+lc0QS3hWCC6CbsZJdhQ23v
7zAKBggqhkjOPQQDAgNHADBEAiAR7UFvg+MjtWcIpNxX5O7KAbZ6/U6JWxH+YNaE
Yb4qGQIgctYTgPJpE+DPbsfM9ZnYgjjE2ZUUUmtyuijpaPIB/Qw=
-----END CERTIFICATE-----`

// GetCorrectOrderAsBytes returns a valid Order object as an array of bytes
func GetCorrectOrderAsBytes(agreementNumber string) []byte {
	invoiceNumber := "1234"
	invoiceDate, _ := time.Parse(time.RFC3339, "2019-11-01T10:02:32Z")
	// paymentDueDate, _ := time.Parse(time.RFC3339, "2019-11-14T10:02:32Z")
	invoiceDateAsStr := invoiceDate.Format("2006-01-02T15:04:05Z")
	invoiceDateAsShortStr := invoiceDate.Format("060102150405")
	// paymentDueDateAsStr := paymentDueDate.Format("2006-01-02T15:04:05Z")
	status := "Initialised"

	// var customer = []byte(`{
	// 	"INN": 		   		"012345678901",
	// 	"PersonalAcc": 		"12345678",
	// 	"Name":        		"TD NLMK"}`)

	// var privateDetails = []byte(`{
	// 	"InvoiceAmount":    2000000,
	// 	"InvoiceVat":		180000,
	// 	"PaymentDetails":	"Payment for the goods",
	// 	"PaymentDueDate":   "` + paymentDueDateAsStr + `",
	// 	"Customer": 	    ` + string(customer) + `}`)

	var privateDetails = []byte(`{}`)

	var order = []byte(`{
		"AgreementNumber":	"` + agreementNumber + `",
		"InvoiceNumber":	"` + invoiceNumber + `",
		"InvoiceDate":      "` + invoiceDateAsStr + `",
		"UIN":				"` + agreementNumber + invoiceNumber + invoiceDateAsShortStr + `",
		"DeliveryAddress":	"484 Garden Ring, Moscow, Russia, 127000",
		"Status":			"` + status + `",
		"PrivateDetails":	` + string(privateDetails) + `}`)

	return order
}

// GetPrivateDetailsAsBytes returns a 'Private Details' object as an array of bytes
func GetPrivateDetailsAsBytes() []byte {

	paymentDueDate, _ := time.Parse(time.RFC3339, "2019-11-14T10:02:32Z")
	paymentDueDateAsStr := paymentDueDate.Format("2006-01-02T15:04:05Z")

	var customer = []byte(`{
		"INN": 		   		"0123456789",
		"PersonalAcc": 		"12345678",
		"Name":        		"TD NLMK"}`)

	var privateDetails = []byte(`{
		"InvoiceAmount":    2000000,
		"InvoiceVat":		180000,
		"PaymentDetails":	"Payment for the goods",
		"PaymentDueDate":   "` + paymentDueDateAsStr + `",
		"Customer": 	    ` + string(customer) + `}`)

	return privateDetails
}
