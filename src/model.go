package main

import "time"

const (
	UPDATE_EXISTING = iota
	SKIP
	THROW
)

// Customer with its details
type Customer struct {
	INN         string `json:"inn"`
	PersonalAcc string `json:"personalAcc"`
	Name        string `json:"name"`
}

// PrivateDetails contains the private deal details available only to the 'nlmk' group members
type PrivateDetails struct {
	InvoiceAmount  float64   `json:"invoiceAmount"`
	InvoiceVat     float64   `json:"invoiceVat"`
	PaymentDetails string    `json:"paymentDetails"`
	PaymentDueDate time.Time `json:"paymentDueDate"`
	Customer       Customer  `json:"customer"`
}

// Order contains public and private details for orders
type Order struct {
	AgreementNumber string         `json:"agreementNumber"`
	InvoiceNumber   string         `json:"invoiceNumber"`
	InvoiceDate     time.Time      `json:"invoiceDate"`
	UIN             string         `json:"uin"`
	TxID            string         `json:"txId"`
	DeliveryAddress string         `json:"deliveryAddress"`
	Status          paymentStatus  `json:"status"`
	CreatedBy       string         `json:"createdBy"`
	CreatedAt       time.Time      `json:"createdAt"`
	UpdatedBy       string         `json:"updatedBy"`
	UpdatedAt       time.Time      `json:"updatedAt"`
	PrivateDetails  PrivateDetails `json:"privateDetails"`
}

// PublicOrder contains only the public part of the Order
type PublicOrder struct {
	AgreementNumber string        `json:"agreementNumber"`
	InvoiceNumber   string        `json:"invoiceNumber"`
	InvoiceDate     time.Time     `json:"invoiceDate"`
	UIN             string        `json:"uin"`
	TxID            string        `json:"txId"`
	DeliveryAddress string        `json:"deliveryAddress"`
	Status          paymentStatus `json:"status"`
	CreatedBy       string        `json:"createdBy"`
	CreatedAt       time.Time     `json:"createdAt"`
	UpdatedBy       string        `json:"updatedBy"`
	UpdatedAt       time.Time     `json:"updatedAt"`
}

// UIN represents the key object for the Order
type UIN struct {
	AgreementNumber string `json:"agreementNumber"`
	InvoiceNumber   string `json:"invoiceNumber"`
	InvoiceDate     string `json:"invoiceDate"`
}

// OrderStatus for obj/json parameter conversions
type OrderStatus struct {
	Status paymentStatus `json:"status"`
}

type paymentStatus string

const (
	Initialised    paymentStatus = "Initialised"
	DraftCreated   paymentStatus = "DraftCreated"
	PaidToBank     paymentStatus = "PaidToBank"
	ApprovedByBank paymentStatus = "ApprovedByBank"
	DeclinedByBank paymentStatus = "DeclinedByBank"
	Finalised      paymentStatus = "Finalised"
)

const (
	// FullInfo collection definiting access to the private data on channel
	FullInfo string = "fullInfo"
	// LogisticOrg reflects the name of the 'logistic' organisation
	LogisticOrg = "logistic"
	// NlmkOrg reflects the name of the 'nlmk' organisation
	NlmkOrg = "nlmk"
	// PermissionList defines access rights of the orgs to the collection
	PermissionList = `
	{
		"` + FullInfo + `": {
		"` + NlmkOrg + `": true,
		"` + LogisticOrg + `": false
		}
	}`

	// StatusSchema defines the sequence in which the status can be changed
	StatusSchema = `
	{
		"` + Initialised + `": {
			"` + DraftCreated + `": true
		},
		"` + DraftCreated + `": {
			"` + PaidToBank + `": true
		},
		"` + PaidToBank + `": {
			"` + ApprovedByBank + `": true,
			"` + DeclinedByBank + `": true
		},
		"` + ApprovedByBank + `": {
			"` + Finalised + `": true
		},
		"` + DeclinedByBank + `": {
			"` + Finalised + `": true
		}
	}`

	prefixOrder = "order"
	nilByte     = "\u0000"
	emptyString = ""
)

// EventID type
type EventID string

// Event struct
type Event struct {
	ID        EventID   `json:"id"`
	CreatedAt time.Time `json:"createdAt"`
}

// OrderInitialisedEvent used by the createOrder method
type OrderInitialisedEvent struct {
	Event
	UIN         string      `json:"uin"`
	OrderStatus OrderStatus `json:"orderStatus"`
}
