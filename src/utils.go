package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/hyperledger/fabric/core/chaincode/lib/cid"
	"github.com/hyperledger/fabric/core/chaincode/shim"
)

// CNFromX509 extracts CN from an x509 certificate
// func CNFromX509(certPEM string) (string, error) {
// 	cert, err := parsePEM(certPEM)
// 	if err != nil {
// 		return "", errors.New("Failed to parse certificate: " + err.Error())
// 	}
// 	return cert.Subject.CommonName, nil
// }

// CallerCN extracts CN from caller of a chaincode function
// func CallerCN(stub shim.ChaincodeStubInterface) (string, error) {
// 	creator, _ := stub.GetCreator()
// 	//Create a SerializedIdentity to hold Unmarshal GetCreator() result
// 	sID := &msp.SerializedIdentity{}
// 	//Unmarshal the creator from []byte to structure
// 	err := proto.Unmarshal(creator, sID)
// 	if err != nil {
// 		return "", errors.New("Could not unmarshal Creator: " + err.Error())
// 	}

// 	cn, err := CNFromX509(string(sID.IdBytes))
// 	if err != nil {
// 		return "", errors.New("Could not get CN from X509: " + err.Error())
// 	}
// 	return cn, nil
// }

// func parsePEM(certPEM string) (*x509.Certificate, error) {
// 	block, rest := pem.Decode([]byte(certPEM))
// 	if block == nil {
// 		return nil, errors.New("Failed to parse PEM certificate" + string(rest))
// 	}

// 	return x509.ParseCertificate(block.Bytes)
// }

// ConvertDateToShortStr returns a short string representation of the date input value
func ConvertDateToShortStr(date time.Time) string {
	return date.Format("060102150405")
}

// ConvertDateToLongStr returns a long string representation of the date input value
func ConvertDateToLongStr(date time.Time) string {
	return date.Format("2006-01-02T15:04:05Z")
}

func validateNumberOfArgumentsWithLengthCheck(expectedNumberOfArguments int, args []string) error {
	err := validateNumberOfArguments(expectedNumberOfArguments, args)
	if err != nil {
		return err
	}

	for i := 0; i < expectedNumberOfArguments; i++ {
		if len(args[i]) == 0 {
			s := []string{"Parameter", strconv.Itoa(i), "is empty but should not be"}
			logger.Error(s)
			return errors.New(strings.Join(s, " "))
		}
	}

	return nil
}

func validateNumberOfArguments(expectedNumberOfArguments int, args []string) error {
	if len(args) != expectedNumberOfArguments {
		s := []string{"Expected", strconv.Itoa(expectedNumberOfArguments), "parameters instead of", strconv.Itoa(len(args))}
		logger.Error(s)
		return errors.New(strings.Join(s, " "))
	}

	return nil
}

func convertArgToPaymentStatus(arg string) (paymentStatus, error) {
	var paymentStatus paymentStatus

	var orderStatus OrderStatus
	payload := []byte(arg)
	err := json.Unmarshal(payload, &orderStatus)
	if err != nil {
		return paymentStatus, errors.New("Could not convert arg " + arg + " to a valid orderStatus")
	}

	switch orderStatus.Status {
	case Initialised:
		return orderStatus.Status, nil
	case DraftCreated:
		return orderStatus.Status, nil
	case PaidToBank:
		return orderStatus.Status, nil
	case ApprovedByBank:
		return orderStatus.Status, nil
	case DeclinedByBank:
		return orderStatus.Status, nil
	case Finalised:
		return orderStatus.Status, nil
	}

	return paymentStatus, errors.New("The Status passed is unknown")
}

func convertStringArgToPaymentStatus(arg string) (paymentStatus, error) {
	var paymentStatus paymentStatus

	switch arg {
	case "Initialised":
		return Initialised, nil
	case "PaidToBank":
		return PaidToBank, nil
	case "DraftCreated":
		return DraftCreated, nil
	case "ApprovedByBank":
		return ApprovedByBank, nil
	case "DeclinedByBank":
		return DeclinedByBank, nil
	case "Finalised":
		return Finalised, nil
	default:
		return paymentStatus, errors.New("The Status passed is unknown")
	}
}

// ConvertPaymentStatusToStr helps with statuses conversions
func ConvertPaymentStatusToStr(arg paymentStatus) (string, error) {
	var result string
	switch arg {
	case Initialised:
		return "Initialised", nil
	case DraftCreated:
		return "DraftCreated", nil
	case PaidToBank:
		return "PaidToBank", nil
	case ApprovedByBank:
		return "ApprovedByBank", nil
	case DeclinedByBank:
		return "DeclinedByBank", nil
	case Finalised:
		return "Finalised", nil
	default:
		return result, errors.New("The Status passed is unknown")
	}
}

func extractOrderFromArgs(stub shim.ChaincodeStubInterface, args []string, transient map[string][]byte) (Order, error) {

	// TODO: Add validation for the order attributes, such as mandatory, length, format, etc...
	// TODO: That's actually quite a big piece of work considering amount of negotiations about format of the fields

	var order Order
	payload := []byte(args[0])
	err := json.Unmarshal(payload, &order)
	if err != nil {
		return order, err
	}

	order.UIN = ConstructUIN(order.AgreementNumber, order.InvoiceNumber, order.InvoiceDate)

	pdAsBytes, ok := transient["privateDetails"]
	if !ok {
		// making a second attempt to get the Private Details presuming we're in test mode (using mock data)
		pdAsBytes, err = stub.GetPrivateData(FullInfo, "privateDetails")
		if err != nil {
			return order, errors.New("Could not extract the Private Details from the transient map")
		}
	}

	if len(pdAsBytes) == 0 {
		return order, errors.New("Private Details must be a non-empty JSON object in the transient map")
	}

	logger.Info("The extracted Private Details object is " + string(pdAsBytes))

	var privateDetails PrivateDetails
	err = json.Unmarshal(pdAsBytes, &privateDetails)
	if err != nil {
		return order, err
	}

	order.PrivateDetails = privateDetails

	err = validateOrder(order)
	if err != nil {
		return order, err
	}

	return order, nil
}

// ConstructUIN creates a UIN based on the AgreementNumber, InvoiceNumber and InvoiceDate fields
func ConstructUIN(agreementNumber string, invoiceNumber string, invoiceDate time.Time) string {
	// construct the UIN by simple concatenation
	sep := ""
	invoiceDateAsStr := ConvertDateToShortStr(invoiceDate)
	slice := []string{agreementNumber, invoiceNumber, invoiceDateAsStr}
	return strings.Join(slice, sep)
}

func extractOrderKeyFromArgs(args []string) ([]string, error) {

	// previous version
	// var orderKey OrderKey
	// orderKey.AgreementNumber = args[0]
	// orderKey.InvoiceNumber = args[1]
	// var invoiceDate time.Time
	// invoiceDate, err := time.Parse(time.RFC3339, args[2])
	// if err != nil {
	// 	return nil, err
	// }
	// orderKey.InvoiceDate = ConvertDateToShortStr(invoiceDate)

	// return []string{orderKey.AgreementNumber, orderKey.InvoiceNumber, orderKey.InvoiceDate}, nil

	// here certain validations of the UIN should happen as it's supposed to be passed as the first element of the array

	return []string{args[0]}, nil
}

func extractOrderKeyFromOrder(order Order) (string, error) {
	// var orderKey UIN
	// orderKey.AgreementNumber = order.AgreementNumber
	// orderKey.InvoiceNumber = order.InvoiceNumber
	// orderKey.InvoiceDate = ConvertDateToShortStr(order.InvoiceDate)

	// return []string{orderKey.AgreementNumber, orderKey.InvoiceNumber, orderKey.InvoiceDate}, nil

	return ConstructUIN(order.AgreementNumber, order.InvoiceNumber, order.InvoiceDate), nil
}

func validateOrder(orderToValidate Order) error {
	// check INN
	err := checkINN(orderToValidate.PrivateDetails.Customer.INN)
	if err != nil {
		return err
	}
	// ... other validations ...
	return nil
}

func checkINN(inn string) error {
	var reINN = regexp.MustCompile(`[0-9]$`)

	if len(inn) == 0 {
		return errors.New("INN is blank")
	} else if !reINN.MatchString(inn) {
		return errors.New("INN must only contain digits")
	} else if len(inn) != 10 {
		return errors.New("INN must consist of 10 digits")
	}
	return nil
}

// func mergeOrderPartsAsBytes(publicBytes []byte, privateBytes []byte) ([]byte, error) {
// 	var retOrder Order

// 	var publicOrder PublicOrder
// 	err := json.Unmarshal(publicBytes, &publicOrder)
// 	if err != nil {
// 		return nil, err
// 	}

// 	var privateDetails PrivateDetails
// 	err = json.Unmarshal(privateBytes, &privateDetails)
// 	if err != nil {
// 		return nil, err
// 	}

// 	retOrder.AgreementNumber = publicOrder.AgreementNumber
// 	retOrder.InvoiceNumber = publicOrder.InvoiceNumber
// 	retOrder.InvoiceDate = publicOrder.InvoiceDate
// 	retOrder.UIN = publicOrder.UIN
// 	retOrder.TxID = publicOrder.TxID
// 	retOrder.DeliveryAddress = publicOrder.DeliveryAddress
// 	retOrder.Status = publicOrder.Status
// 	retOrder.CreatedBy = publicOrder.CreatedBy
// 	retOrder.CreatedAt = publicOrder.CreatedAt
// 	retOrder.UpdatedBy = publicOrder.UpdatedBy
// 	retOrder.UpdatedAt = publicOrder.UpdatedAt
// 	retOrder.PrivateDetails = privateDetails

// 	retOrderBytes, _ := json.Marshal(retOrder)

// 	return retOrderBytes, nil
// }

func mergeOrderPartsAsObject(publicBytes []byte, privateBytes []byte) (Order, error) {
	var retOrder Order

	var publicOrder PublicOrder
	err := json.Unmarshal(publicBytes, &publicOrder)
	if err != nil {
		return retOrder, err
	}

	var privateDetails PrivateDetails
	err = json.Unmarshal(privateBytes, &privateDetails)
	if err != nil {
		return retOrder, err
	}

	retOrder.AgreementNumber = publicOrder.AgreementNumber
	retOrder.InvoiceNumber = publicOrder.InvoiceNumber
	retOrder.InvoiceDate = publicOrder.InvoiceDate
	retOrder.UIN = publicOrder.UIN
	retOrder.TxID = publicOrder.TxID
	retOrder.DeliveryAddress = publicOrder.DeliveryAddress
	retOrder.Status = publicOrder.Status
	retOrder.CreatedBy = publicOrder.CreatedBy
	retOrder.CreatedAt = publicOrder.CreatedAt
	retOrder.UpdatedBy = publicOrder.UpdatedBy
	retOrder.UpdatedAt = publicOrder.UpdatedAt
	retOrder.PrivateDetails = privateDetails

	return retOrder, nil
}

// ValidateAccess checks the access rights to the private info according to the collection config
func ValidateAccess(stub shim.ChaincodeStubInterface) (string, error) {

	mspID, err := cid.GetMSPID(stub)
	if err != nil {
		return emptyString, errors.New("Could not determine caller's MSP Id")
	}

	cert, err := cid.GetX509Certificate(stub)
	if err != nil {
		str := "Could not get an X509 certificate associated with the call"
		logger.Error(str + err.Error())
		return emptyString, errors.New(str)
	}

	var createdBy string
	if cert.Subject.CommonName != "" {
		createdBy = cert.Subject.CommonName
	} else {
		msg := "The certificate does not contain the caller's name"
		logger.Error(msg)
		return emptyString, errors.New(msg)
	}

	// check the users' permissions
	if access, err := AccessControl(FullInfo, mspID); err != nil {
		return emptyString, errors.New(err.Error())
	} else {
		if !access {
			return emptyString, errors.New("Unauthorised")
		}
	}

	return createdBy, nil
}

// AccessControl checks control of a specific mspID to a specific collection
// according to the permissionList predefined globally
func AccessControl(collection string, mspID string) (bool, error) {
	var permList map[string]map[string]bool
	permissionListAsByte := []byte(PermissionList)
	if err := json.Unmarshal(permissionListAsByte, &permList); err != nil {
		return false, err
	}
	// check if the collection exists
	if permission, exist := permList[collection]; !exist {
		return false, nil
		// check if the users are from the organizations which have access permission
	} else if permission[mspID] {
		return true, nil
	}

	return false, nil
}

// ValidateStatusChange checks validity of Order status change from to according to the respective arguments
func ValidateStatusChange(fromStatus paymentStatus, toStatus paymentStatus) (bool, error) {
	var statusSchema map[paymentStatus]map[paymentStatus]bool
	statusSchemaAsByte := []byte(StatusSchema)
	if err := json.Unmarshal(statusSchemaAsByte, &statusSchema); err != nil {
		return false, err
	}

	// check if the collection exists
	if fromStatus, exist := statusSchema[fromStatus]; !exist {
		return false, nil
		// check if the users are from the organizations which have access permission
	} else if fromStatus[toStatus] {
		return true, nil
	}

	return false, nil
}

func queryLedgerByStatus(stub shim.ChaincodeStubInterface, args []string) (shim.StateQueryIteratorInterface, error) {
	err := validateNumberOfArgumentsWithLengthCheck(1, args)
	if err != nil {
		return nil, err
	}

	key := args[0]

	// Get iterator by range
	accountIterator, err := getIteratorByStatusQuery(stub, key)
	if err != nil {
		return nil, err
	}

	return accountIterator, nil
}

func getIteratorByStatusQuery(stub shim.ChaincodeStubInterface, key string) (shim.StateQueryIteratorInterface, error) {
	queryString := fmt.Sprintf("{\"selector\":{\"status\":\"%s\"}}", key)
	logger.Debug("queryString is " + queryString)
	itemIterator, err := stub.GetQueryResult(queryString)

	if err != nil {
		return nil, err
	}
	defer itemIterator.Close()

	return itemIterator, nil
}

func listOrdersFromIterator(stub shim.ChaincodeStubInterface, ordersIterator shim.StateQueryIteratorInterface) ([]byte, error) {
	var orders []Order
	for ordersIterator.HasNext() {
		var mergedOrder Order
		orderItem, err := ordersIterator.Next()
		if err != nil {
			return nil, err
		}

		itemBytes, err := readElementFromLedger(stub, orderItem.Key)

		itemPrivateBytes, err := readElementFromPrivateData(stub, orderItem.Key)

		mergedOrder, err = mergeOrderPartsAsObject(itemBytes, itemPrivateBytes)
		if err != nil {
			return nil, err
		}

		orders = append(orders, mergedOrder)
	}

	ordersAsBytes, err := json.Marshal(orders)
	if err != nil {
		return nil, err
	}

	return ordersAsBytes, nil
}

func getOrderHistoryAsBytes(stub shim.ChaincodeStubInterface, orderKey string) ([]byte, error) {
	type AuditHistory struct {
		TxID  string `json:"txID"`
		Value Order  `json:"value"`
	}

	var history []AuditHistory
	var order Order
	var privateDetails PrivateDetails

	orderPrivateBytes, err := readElementFromPrivateData(stub, orderKey)
	_ = json.Unmarshal(orderPrivateBytes, &privateDetails)

	// Get History
	resultsIterator, err := stub.GetHistoryForKey(orderKey)
	if err != nil {
		return nil, errors.New("Error when getting an iterator for order history")
	}

	defer resultsIterator.Close()

	for resultsIterator.HasNext() {

		historyData, err := resultsIterator.Next()
		if err != nil {
			return nil, errors.New("Error when getting history data for an order")
		}
		var tx AuditHistory
		tx.TxID = historyData.TxId
		json.Unmarshal(historyData.Value, &order)
		order.PrivateDetails = privateDetails
		tx.Value = order

		history = append(history, tx)
	}

	historyBytes, _ := json.Marshal(history)

	return historyBytes, nil
}

func raiseOrderInitEvent(stub shim.ChaincodeStubInterface, order Order) error { // status OrderStatus

	// TODO: implement a generic version of the method once the requirements are clarified

	ID := fmt.Sprintf("OE_%s_%s", order.UIN, ConvertDateToLongStr(order.CreatedAt))

	var status OrderStatus
	status.Status = Initialised
	var event OrderInitialisedEvent
	event.ID = EventID(ID)
	event.CreatedAt = order.CreatedAt
	event.UIN = order.UIN
	event.OrderStatus = status
	eventAsBytes, _ := json.Marshal(event)

	eventAsStr, err := ConvertPaymentStatusToStr(Initialised) // status.Status
	if err != nil {
		return err
	}

	if err := stub.SetEvent(eventAsStr, eventAsBytes); err != nil {
		return err
	}

	logger.Info("Raised the Init event: " + string(eventAsBytes))

	return nil
}
