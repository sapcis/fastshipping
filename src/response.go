package main

import pb "github.com/hyperledger/fabric/protos/peer"

const (
	OK             = int32(200)
	CREATED        = int32(201)
	ACCEPTED       = int32(202)
	BAD_REQUEST    = int32(400)
	NOT_AUTHORISED = int32(401)
	NOT_FOUND      = int32(404)
	CONFLICT       = int32(409)
	ERROR          = int32(500)
)

func Ok(payload []byte) pb.Response {
	return pb.Response{
		Status:  OK,
		Payload: payload,
	}
}

func Created(payload []byte) pb.Response {
	return pb.Response{
		Status:  CREATED,
		Payload: payload,
	}
}

func NotFound(msg string) pb.Response {
	return pb.Response{
		Status:  NOT_FOUND,
		Message: msg,
	}
}

func BadRequest(msg string) pb.Response {
	return pb.Response{
		Status:  BAD_REQUEST,
		Message: msg,
	}
}

func Conflict(msg string) pb.Response {
	return pb.Response{
		Status:  CONFLICT,
		Message: msg,
	}
}

func Error(msg string) pb.Response {
	return pb.Response{
		Status:  ERROR,
		Message: msg,
	}
}

func NotAuthorised(msg string) pb.Response {
	return pb.Response{
		Status:  NOT_AUTHORISED,
		Message: msg,
	}
}
